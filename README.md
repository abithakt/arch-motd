# Arch MOTD

My attempt at a dynamic MOTD on Arch Linux. More details at <https://abithakt.gitlab.io/2019/dynamic-motd-on-arch/>.

If you try this out, send me a picture of what you come up with -- I'd love to see.

## Preview

<center>
![light text on a dark background, giving system information like storage space, WiFi, battery status, Linux kernel version; as well as the date, upcoming calendar events, and a quote which reads 'Talent is a pursued interest. In other words, anything that you're willing to practice, you can do. -- Bob Ross'](https://gitlab.com/abithakt/arch-motd/-/raw/master/motd.png)
</center>

## Prerequisites

- `zsh`
- `toilet`
- `awk`
- `df`
- `sed`
- `when`
- `shuf`
- `fmt`
- a file of quotes at `/opt/quotes.md`, one quote per line, with quote and attribution separated by ` --- `; or, alternatively, `fortune`
- get the colour scheme file from [`pywal`](https://github.com/dylanaraps/pywal) or write it yourself

## Usage

1. Preliminary set-up:
```bash
git clone https://gitlab.com/abithakt/arch-motd
cd arch-motd
chmod +x motdmaker
mv motdmaker /usr/bin/motdmaker
mv colouredline /opt/
```

2. Do some editing, [as outlined at the end of this post](https://abithakt.gitlab.io/2019/dynamic-motd-on-arch/)

3. Call it at the end of your `/etc/profile`:
```bash
if [[ ! $TMUX ]]; then
	/usr/bin/motdmaker
	cat /opt/motd
fi
```
